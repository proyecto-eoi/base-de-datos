DROP DATABASE IF EXISTS perretes_competiciones;
CREATE DATABASE perretes_competiciones WITH TEMPLATE = template0 ENCODING = 'UTF8';
	
ALTER DATABASE perretes_competiciones OWNER TO postgres;
\connect perretes_competiciones;

CREATE TABLE categoria (
    id SERIAL NOT NULL,
    nombre character varying COLLATE pg_catalog."default",
    CONSTRAINT categoria_pkey PRIMARY KEY (id)
);

ALTER TABLE categoria OWNER TO postgres;

CREATE TABLE modalidad (
    id SERIAL NOT NULL,
    nombre character varying COLLATE pg_catalog."default",
    url character varying COLLATE pg_catalog."default",
    CONSTRAINT modalidad_pkey PRIMARY KEY (id)
);

ALTER TABLE modalidad OWNER TO postgres;

CREATE TABLE perros (
    id SERIAL NOT NULL,
    nombre character varying COLLATE pg_catalog."default",
    raza character varying COLLATE pg_catalog."default",
    url character varying COLLATE pg_catalog."default",
    id_categoria integer,
    CONSTRAINT perros_pkey PRIMARY KEY (id),
    CONSTRAINT id_categoria FOREIGN KEY (id_categoria)
        REFERENCES categoria (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
);

ALTER TABLE perros OWNER TO postgres;

CREATE TABLE participa (
	id SERIAL NOT NULL,
    id_perro integer NOT NULL,
    id_modalidad integer NOT NULL,
    fecha date,
    CONSTRAINT participa_pkey PRIMARY KEY (id),
    CONSTRAINT id_modalidad FOREIGN KEY (id_modalidad)
        REFERENCES modalidad (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT id_perro FOREIGN KEY (id_perro)
        REFERENCES perros (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
);

ALTER TABLE participa OWNER TO postgres;

CREATE TABLE tiene (
	id SERIAL NOT NULL,
    id_categoria integer NOT NULL,
    id_modalidad integer NOT NULL,
    CONSTRAINT tiene_pkey PRIMARY KEY (id),
    CONSTRAINT categoria FOREIGN KEY (id_categoria)
        REFERENCES categoria (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT modalidad FOREIGN KEY (id_modalidad)
        REFERENCES modalidad (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
);

ALTER TABLE tiene OWNER TO postgres;

INSERT INTO categoria(nombre)
    VALUES ('Grande'),('Mediano'),('Chico');
INSERT INTO modalidad(nombre, url)
    VALUES ('Freesbee','https://upload.wikimedia.org/wikipedia/commons/9/9e/Dogfrisbee.jpg'),('Obstaculos','https://upload.wikimedia.org/wikipedia/commons/thumb/d/d2/Border_collie_weave_poles.jpg/150px-Border_collie_weave_poles.jpg'),('Natacion','https://upload.wikimedia.org/wikipedia/commons/9/93/20060128DalmenyOtto_002.jpg'),('Canicross','https://upload.wikimedia.org/wikipedia/commons/3/3c/Racheal_Bailey_of_Akna_K9_Academy_with_two_of_her_running_dogs.jpg');
INSERT INTO perros(
    nombre, raza, url, id_categoria)
    VALUES ('Antonio', 'Pastor aleman', 'https://upload.wikimedia.org/wikipedia/commons/9/94/Cane_da_pastore_tedesco_adulto.jpg', 1),('Pepinillo', 'Perro salchicha', 'https://upload.wikimedia.org/wikipedia/commons/2/27/Short-haired-Dachshund.jpg', 2),('Falete', 'Chihuahua', 'https://upload.wikimedia.org/wikipedia/commons/b/b8/Degaen.jpg', 3),('Benito', 'Husky siberiano', 'https://upload.wikimedia.org/wikipedia/commons/thumb/c/ca/Siberian-husky.jpg/245px-Siberian-husky.jpg', 1);
INSERT INTO participa(
    id_perro, id_modalidad, fecha)
    VALUES (1, 3, '2020-02-14'),(2, 1, '2019-03-21'),(3, 3,'2019-03-20');
INSERT INTO participa(
    id_perro, id_modalidad, fecha)
    VALUES (1, 3, '2020-02-14'),(2, 1, '2019-03-21'),(3, 3,'2019-03-20');
INSERT INTO participa(
    id_perro, id_modalidad, fecha)
    VALUES (1, 3, '2020-02-14'),(2, 1, '2019-03-21'),(3, 3,'2019-03-20'),(4, 2,'2019-03-20'),(1, 4,'2019-03-20'),(2, 4,'2019-03-20'),(4, 4,'2019-03-20');
INSERT INTO tiene(
    id_categoria, id_modalidad)
    VALUES (1, 1),(2, 2),(3, 3),(1, 4);